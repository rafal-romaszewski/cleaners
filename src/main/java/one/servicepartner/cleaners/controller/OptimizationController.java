package one.servicepartner.cleaners.controller;

import one.servicepartner.cleaners.model.OptimizationRequest;
import one.servicepartner.cleaners.model.OptimizationResponse;
import one.servicepartner.cleaners.service.CleanersOptimizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class OptimizationController {

    @Autowired
    CleanersOptimizationService service;

    @RequestMapping("/optimize")
    @ResponseBody
    public List<OptimizationResponse> optimize(@RequestBody OptimizationRequest request) {
       return service.optimize(request);
    }
}
