package one.servicepartner.cleaners;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
@EnableAutoConfiguration
public class CleanersOptimizationApp {

	public static void main(String[] args) {
		SpringApplication.run(CleanersOptimizationApp.class, args);
	}
}
