package one.servicepartner.cleaners.service;

import one.servicepartner.cleaners.model.OptimizationResponse;

public class GenericOptimalSplitFinder implements OptimalSplitFinder {

    @Override
    public OptimizationResponse findSplit(long roomSize, long seniorCapacity, long juniorCapacity) {
        if (roomSize % juniorCapacity == 0) {
            return new OptimizationResponse(0L, roomSize / juniorCapacity);
        }
        long currentSenior = 0;
        long currentJunior = roomSize / juniorCapacity + 1;
        long currentExcess = currentJunior * juniorCapacity - roomSize;
        long bestExcess = currentExcess;
        OptimizationResponse bestResponse = new OptimizationResponse(currentSenior, currentJunior);
        while (currentJunior > 0 || currentExcess < 0) {
            if (currentExcess == 0) {
                break;
            } else if (currentExcess > 0) {
                currentJunior--;
            } else if (currentExcess < 0) {
                currentSenior++;
            }
            currentExcess = currentSenior * seniorCapacity + currentJunior * juniorCapacity - roomSize;
            if (currentExcess >= 0 && currentExcess < bestExcess) {
                bestResponse = new OptimizationResponse(currentSenior, currentJunior);
                bestExcess = currentExcess;
            }
        }
        return bestResponse;
    }
}
