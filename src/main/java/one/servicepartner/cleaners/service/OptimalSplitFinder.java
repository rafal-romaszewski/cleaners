package one.servicepartner.cleaners.service;

import one.servicepartner.cleaners.model.OptimizationResponse;

public interface OptimalSplitFinder {

    OptimizationResponse findSplit(long roomSize, long seniorCapacity, long juniorCapacity);
}
