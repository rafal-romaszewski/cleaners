package one.servicepartner.cleaners.service;

import one.servicepartner.cleaners.model.OptimizationRequest;
import one.servicepartner.cleaners.model.OptimizationResponse;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CleanersOptimizationService {

    private OptimalSplitFinder splitFinder = new AtLeastOneSenior_Decorator(new GenericOptimalSplitFinder());

    public List<OptimizationResponse> optimize(OptimizationRequest request) {
        return Arrays.stream(request.getRooms())
                .mapToObj(roomSize -> splitFinder.findSplit(roomSize, request.getSenior(), request.getJunior()))
                .collect(Collectors.toList());
    }
}
