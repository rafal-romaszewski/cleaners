package one.servicepartner.cleaners.service;

import one.servicepartner.cleaners.model.OptimizationResponse;

public class AtLeastOneSenior_Decorator implements OptimalSplitFinder {

    private final OptimalSplitFinder decoratedSolution;

    public AtLeastOneSenior_Decorator(OptimalSplitFinder decoratedSolution) {
        this.decoratedSolution = decoratedSolution;
    }
    
    @Override
    public OptimizationResponse findSplit(long roomSize, long seniorCapacity, long juniorCapacity) {
        OptimizationResponse originalResponse = decoratedSolution.findSplit(roomSize - seniorCapacity, seniorCapacity, juniorCapacity);
        return new OptimizationResponse(originalResponse.getSenior() + 1, originalResponse.getJunior());
    }
}
