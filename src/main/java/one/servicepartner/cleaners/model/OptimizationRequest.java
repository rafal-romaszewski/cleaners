package one.servicepartner.cleaners.model;

import lombok.Data;

@Data
public class OptimizationRequest {
    long[] rooms;
    long senior;
    long junior;
}
