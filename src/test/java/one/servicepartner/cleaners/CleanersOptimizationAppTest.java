package one.servicepartner.cleaners;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.net.URI;
import java.net.URISyntaxException;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CleanersOptimizationApp.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CleanersOptimizationAppTest {

    @Autowired
    private TestRestTemplate rest;

    @LocalServerPort
    int randomServerPort;

    @Test
    public void acceptanceTestCase1() throws URISyntaxException {
        String request = "{ \"rooms\": [35, 21, 17, 28], \"senior\": 10, \"junior\": 6 }";
        String expectedResponse = "[{\"senior\":3,\"junior\":1},{\"senior\":1,\"junior\":2},{\"senior\":2,\"junior\":0},{\"senior\":1,\"junior\":3}]";

        ResponseEntity<String> response = postForResponse(request);

        Assert.assertEquals(expectedResponse, response.getBody());
    }

    @Test
    public void acceptanceTestCase2() throws URISyntaxException {
        String request = "{ \"rooms\": [24, 28], \"senior\": 11, \"junior\": 6 }";
        String expectedResponse = "[{\"senior\":2,\"junior\":1},{\"senior\":2,\"junior\":1}]";

        ResponseEntity<String> response = postForResponse(request);

        Assert.assertEquals(expectedResponse, response.getBody());
    }

    private ResponseEntity<String> postForResponse(String requestBody) throws URISyntaxException {
        final URI baseUri = new URI("http://localhost:" + randomServerPort + "/optimize");

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> request = new HttpEntity<>(requestBody, headers);

        return rest.postForEntity(baseUri, request, String.class);
    }


}
