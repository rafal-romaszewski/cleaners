package one.servicepartner.cleaners.service;

import one.servicepartner.cleaners.model.OptimizationResponse;
import org.junit.Assert;
import org.junit.Test;

public class GenericOptimalSplitFinderTest {

    GenericOptimalSplitFinder splitFinder = new GenericOptimalSplitFinder();

    @Test
    public void shouldReturnJustJuniorsOnMultipleOfJuniorCapacity() {
        Assert.assertEquals(new OptimizationResponse(0, 7),
                splitFinder.findSplit(49, 11, 7));
    }

    @Test
    public void shouldReturnResultWithNoExcessIfPossible_1() {
        Assert.assertEquals(new OptimizationResponse(0, 3),
                splitFinder.findSplit(21, 11, 7));
    }

    @Test
    public void shouldReturnResultWithNoExcessIfPossible_2() {
        Assert.assertEquals(new OptimizationResponse(2, 0),
                splitFinder.findSplit(22, 11, 7));
    }

    @Test
    public void shouldReturnResultWithMinimalExcess() {
        Assert.assertEquals(new OptimizationResponse(1, 2),
                splitFinder.findSplit(23, 11, 7));
    }
}
