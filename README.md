## Running the application
#### From The IDE
Execute the main method from the one.servicepartner.cleaners.CleanersOptimizationApp
#### By Maven
Run mvn spring-boot:run

## Using the application
With default configuration, the application will expose an endpoint at http://localhost:8080/optimize

You can query this endpoint, using for example Postman, providing body in specified format:

```
{
  "rooms": [
    35,
    21,
    17,
    28
  ],
  "senior": 10,
  "junior": 6
}
```

to get a JSON response:

```
[
  {
    "senior": 3,
    "junior": 1
  },
  {
    "senior": 1,
    "junior": 2
  },
  {
    "senior": 2,
    "junior": 0
  },
  {
    "senior": 1,
    "junior": 3
  }
]
```